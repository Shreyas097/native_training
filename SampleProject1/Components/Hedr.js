/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  View,
} from 'react-native';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons'

function Hedr({navigation}) {
  return (
    <View>
      <MaterialIcon name='menu' size={28} onPress={()=>navigation.openDrawer()} />      
    </View>
  );
}

export default Hedr;