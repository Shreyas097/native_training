/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  View,
  Text,
} from 'react-native';

function About({navigation}) {
  return (
    <View>
      <Text style={{margin : 10,fontSize:20}} >The working principles of React Native are virtually identical to React except that React Native does not manipulate the DOM via the Virtual DOM. </Text>
      <Text style={{margin : 10,fontSize:20}} >It runs in a background process (which interprets the JavaScript written by the developers) directly on the end-device and communicates with the native platform via a serialisation, asynchronous and batched Bridge.</Text>
    </View>
  );
}

export default About;
