import React, { useEffect, useRef, useState } from 'react';
import {  
  View,
  Text,
} from 'react-native';
import { FlatList } from 'react-native-gesture-handler';

function Settings({navigation}){
    const a=['About','Profile'];
    return (
        <View>        
           <FlatList 
                data={a}
                renderItem={({item}) => 
                <Text style={{ color:'white',textAlign: 'center',height: 70, borderColor: 'gray', borderWidth: 1,width:300,margin : 10,textAlignVertical: 'center',flex:1,fontSize:25,backgroundColor:'gray'}} onPress={() => navigation.navigate(item)} >{item}</Text>
              }        
            />
        </View>
      );

 
}
export default Settings;