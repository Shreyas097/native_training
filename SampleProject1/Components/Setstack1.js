import { createStackNavigator } from 'react-navigation-stack';
import {createAppContainer,navigationOptions} from 'react-navigation';
import React from 'react';
import Home from './Home'
import Hedr from './Hedr'
const screens={
    Home:{
        screen: Home,
        navigationOptions:({navigation})=>{
            return{
            headerLeft: () => <Hedr navigation={navigation} />,
        }
        }
    }
    }    

const stacks1=createStackNavigator(screens);

export default stacks1;
