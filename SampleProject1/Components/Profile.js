/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useEffect, useRef, useState,Component } from 'react';
import {
  View,
  Text,
} from 'react-native';

import AsyncStorage from '@react-native-async-storage/async-storage';



function Profile({navigation}) {   

  const [name, setName] = React.useState('Not Set');

  useEffect(()=>{storeData()})
  useEffect(()=>{getData()})

  const storeData = async () => {
    try {
      if(navigation.getParam('name')!=null)
        await AsyncStorage.setItem('pname',navigation.getParam('name'));          
    } catch(e) {
      console.log('Error in storing item in async storage ');
    }
  }

  const getData = async () => {
    try {
      
      const val1 = await AsyncStorage.getItem('pname');      
      if(val1 !== null) {
        setName(val1);
      }
    } catch(e) {
      console.log('Error in retrieving item from async storage ');
    }
  }
  
  return (
    <View>     
      <Text style={{margin : 10,fontSize:25}}>Name : {name}</Text>     
    </View>
  );
}

export default Profile;
