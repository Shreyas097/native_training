/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';


import { WebView } from 'react-native-webview'



function Search({navigation}) {   
  return (
    <WebView      
      startInLoadingState
      style={{ flex: 1 }}
      source={{ uri: "https://google.com" }}
    />
  );
}

export default Search;
