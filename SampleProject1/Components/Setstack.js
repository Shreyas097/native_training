import { createStackNavigator } from 'react-navigation-stack';
import {createAppContainer,navigationOptions} from 'react-navigation';
import About from './About';
import Profile from './Profile';
import Settings from './Settings';
import Editprofile from './Editprofile';
import React from 'react';
import { Button } from 'react-native-elements'
import Hedr from './Hedr'
const screens={
    Settings:{
        screen: Settings,
        navigationOptions:({navigation})=>{
            return{
            headerLeft: () => <Hedr navigation={navigation} />,
        }
        }
    },
    About:{
        screen: About
    },
    Profile:{
        screen: Profile,
        
        navigationOptions:({navigation})=>{
            return{
            headerRight: () => (
            <Button
              onPress={()=>navigation.navigate('Editprofile')}
              title="EDIT"
                containerStyle={{ backgroundColor: 'transparent', marginRight: 15 }}
                buttonStyle={{ backgroundColor: 'transparent' }}
                titleStyle={{ color: 'blue'}}
            />
          )}
        } 
          
    },
    Editprofile:{
        screen: Editprofile
    }
}
const stacks=createStackNavigator(screens);

export default stacks;