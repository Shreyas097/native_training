import { createDrawerNavigator } from 'react-navigation-drawer';
import {createAppContainer,navigationOptions} from 'react-navigation';
import stacks from './Setstack';
import stacks1 from './Setstack1';
import Hedr from './Hedr'
import React from 'react';
const screens={
    Home:{
        screen: stacks1,
        navigationOptions:({navigation})=>{
            return{
            headerTitle: () => <Hedr navigation={navigation} />,
        }
        }
        },
    Settings:{
        screen: stacks,
        }
        
    }

    
const dra=createDrawerNavigator(screens);


const dapc=createAppContainer(dra);

export default dapc;