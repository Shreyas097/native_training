/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useEffect, useRef, useState,Component } from 'react';
import {
  View,
  Text,
} from 'react-native';
import { Button } from 'react-native-elements'
import axios from 'axios'

function Weather({navigation}) {   

  const [wea, setWea] = React.useState('Not Avialable');

  const fetch =async()=>
    {
        const response = await axios.get('https://api.openweathermap.org/data/2.5/weather?q=London&appid=803f421516fca166f9c67c9cfabdc1eb');        
        const w=response.data.weather[0].description;
        console.log("Response is : ",w);
        setWea(w);
    } 
  
  return (
    <View>
        <Button
            onPress={() => fetch()}
            title="Fetch Weather Data"
            />   
        <Text style={{margin : 10,fontSize:25}}>Weather : {wea}</Text>     
    </View>
  );
}

export default Weather;
