import { createBottomTabNavigator } from 'react-navigation-tabs';
import {createAppContainer,navigationOptions} from 'react-navigation';
import Weather from './Weather';
import Search from './Search';
import React from 'react';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
const screens={
    Weather:{
        screen: Weather,
        navigationOptions:{
            tabBarIcon:()=>(<MaterialCommunityIcons name="weather-cloudy"  size={26} />)
        }
    },
    Search:{
        screen: Search,
        navigationOptions:{
            tabBarIcon:()=>(<MaterialCommunityIcons name="search-web"  size={26} />)
        }
    }
}
    
const tabs=createBottomTabNavigator(screens);

const tapc=createAppContainer(tabs);

export default tapc;