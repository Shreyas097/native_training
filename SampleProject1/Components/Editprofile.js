/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component, useEffect, useRef, useState } from 'react';
import {
  View,
  TextInput,
} from 'react-native';
import { Button } from 'react-native-elements'
import AsyncStorage from '@react-native-async-storage/async-storage';



function Editprofile({navigation}){

  const [value, onChangeText] = React.useState('Not Set'); 
  useEffect(()=>{getData()},[])

  const sendData =() => {    
      navigation.navigate('Profile',{name:value})
  } 
  const getData = async () => {
    try {      
      const val1 = await AsyncStorage.getItem('pname');      
      if(val1 !== null) {
        onChangeText(val1);
      }
    } catch(e) {
      console.log('Error in retrieving item from async storage ');
    }
  }
  
  return (      
    <View>
      <TextInput
          style={{ height: 50, borderColor: 'gray', borderWidth: 1,width:300,margin : 10,fontSize:25,backgroundColor:'white'}}
          onChangeText={text => onChangeText(text)}
          value={value}
        />
        <Button
          onPress={sendData}
          title="SAVE"          
        />          
    </View>   
  );
}

export default Editprofile;
