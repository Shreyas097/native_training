import React,{ Component } from 'react';
import { TextInput } from 'react-native';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Button,
  StatusBar,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import * as EmailValidator from 'email-validator';

const Headr = () => {
  
  return (
      <View >
          <Text style={styles.sectionTitle}>React Native Training</Text>       
    </View>
  );
}

const styles = StyleSheet.create({
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
    padding:10,
  },
});

export default Headr;