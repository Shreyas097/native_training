/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import { TextInput } from 'react-native';
import {
  View,
  Button,
} from 'react-native';
import * as EmailValidator from 'email-validator';


const Emlval = () => {
  const [value, onChangeText] = React.useState('');

  function validateEmail()
  {
    if(EmailValidator.validate(value))
    {
      alert("Valid email address!");
    }
    else
    {
      alert("Invalid email address!");
    }
  };
  return (   
    
      <View style={{alignItems: 'center',justifyContent: 'center',flex:1,backgroundColor:'#b4b4b4' }}>      
        <TextInput
          style={{ height: 50, borderColor: 'gray', borderWidth: 1,width:300,margin : 10,fontSize:25,backgroundColor:'white'}}
          onChangeText={text => onChangeText(text)}
          value={value}
        />
        <Button
          onPress={validateEmail}
          title="Test"          
        />      
      </View>    
  );
}

export default Emlval;
