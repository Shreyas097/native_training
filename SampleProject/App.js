/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React,{ Component } from 'react';
import { TextInput } from 'react-native';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Button,
  StatusBar,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import Headr from './components/Headr'
import Emlval from './components/Emlval'

const App = () => {
  return (   
    <View style={{flex:1 }}>  
      <Headr/> 
      <Emlval/>   
    </View>
  );
}

export default App;
